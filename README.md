# Ecomdy admin template

This is a template for administration bases on [Vite](https://vitejs.dev/guide/), [Vue](https://vuejs.org/), [Pinia](https://pinia.vuejs.org/), [Antd](https://antdv.com/components/overview).

[Demo](https://main--capable-pothos-851385.netlify.app/)


## Project Setup

```sh
yarn install
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Type-Check, Compile and Minify for Production

```sh
yarn build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
yarn test:e2e:dev
```

This runs the end-to-end tests against the Vite development server.
It is much faster than the production build.

But it's still recommended to test the production build with `test:e2e` before deploying (e.g. in CI environments):

```sh
yarn build
yarn test:e2e
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```


--------------
## Structure project
```bash
├── src                    
│   ├── assets              # Dynamic assets
│   ├── components          # components used in pages & layouts
|   ├── layouts             # contains layout views component
|   ├── libs                # contains 3rd package
|   ├── locales             # language for app
|   ├── modules             # every module should be here
|   ├── pages               # page of application
|   ├── router              # config routers for app
|   ├── services            # contain services to fetch api
|   ├── stores              # global state management use Pinia
|   ├── utils               # helper functions

```

---------------
