import { routes } from '../../router';

export interface MenuListType {
  path: string;
  icon?: Function;
  name: string;
}

const menuList: Array<MenuListType> = [];
routes.forEach(route => {
  const isChildPage = route.path.substring(1).includes('/');
  if (route.hidden || isChildPage) return;

  menuList.push(
    {
      name: route.name,
      path: route.path,
      icon: route.icon,
    }
  )
});

export {
  menuList
}