// Using
// import CustomIcon from '@/components/icon/customIcon';
// import vueIcon from "@/assets/vue.svg";

// const VueIcon = CustomIcon({
//   src: vueIcon,
// });

import { h } from 'vue';
import './icon.scss';

const CustomIcon = (props: any) => {
  return h(
    "img",
    {
      ...props,
      class: `anticon custom-icon ${props.class}`,
    },
  )
};

CustomIcon.props = ['src'];

export default CustomIcon;