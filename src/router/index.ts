import { createRouter, createWebHistory } from 'vue-router';
import * as modules from '../modules'
import { DashboardOutlined } from '@ant-design/icons-vue';


const appModules = Object.values(modules.default);

interface RouteType {
    path: string;
    component: Function;
    name: string;
    icon?: Function;
    hidden?: boolean;
  }

const routes: Array<RouteType>  = [
    { 
        path: "/:catchAll(.*)",
        name: '404 Page',
        component: () => import('@/pages/error/NotFound.vue'),
        hidden: true,
    },
    {
        path: '/',
        component: () => import('@/pages/dashboard/Dashboard.vue'),
        name: 'Dashboard',
        icon: DashboardOutlined,
        hidden: false,
    },

    {
        path: '/login',
        component: () => import('../pages/login/Login.vue'),
        name: 'Login',
        hidden: true,
    }
]

appModules.forEach(module => {
    Object.values(module.routes).forEach((route): void => {
        routes.push({
            path: `/${module.name}${route.path}`,
            component: route.component,
            name: route.name,
            icon: module.icon,
        })
    })
});

// @ts-ignore
const router = createRouter({ history: createWebHistory(import.meta.env.BASE_URL), routes })
export default router;
export {
    routes,
};