import { ref, computed } from 'vue';
import { defineStore } from 'pinia';

export const useUserStore = defineStore('user', () => {
  const users = ref([]);
  const getUsers = computed(() => users.value);
  function addUser() {
  }

  return { users, getUsers, addUser };
})
