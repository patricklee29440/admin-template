import {
    EuroOutlined,
  } from '@ant-design/icons-vue';

const userRoutes = {
    name: 'user',
    icon: EuroOutlined,
    routes: {
        index: {
            component: () => import('./views/User.vue'),
            path: '',
            name: 'User',
        },
    },
}

export {
    userRoutes
}