import {
    EuroOutlined,
  } from '@ant-design/icons-vue';

const componentRoutes = {
    name: 'component',
    icon: EuroOutlined,
    routes: {
        index: {
            component: () => import('./views/Component.vue'),
            path: '',
            name: 'Components',
        },
    },
}

export {
    componentRoutes
}