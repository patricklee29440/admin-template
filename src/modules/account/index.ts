import {
    EuroOutlined,
  } from '@ant-design/icons-vue';

const accountRoutes = {
    name: 'account',
    icon: EuroOutlined,
    routes: {
        index: {
            component: () => import('./views/Account.vue'),
            path: '',
            name: 'Account',
        },
    },
}

export {
    accountRoutes
}