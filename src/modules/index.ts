import { userRoutes } from './user';
import { accountRoutes } from './account';
import { componentRoutes } from './component';

const modules = [
    userRoutes,
    accountRoutes,
    componentRoutes
];

export default modules;