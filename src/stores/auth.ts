import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import AuthService from '@/services/auth';
import libAuth from '@/libs/auth';
import { getResponse } from '@/utils/response';

const useAuthStore = defineStore('auth', () => {
  const auth = ref({
    status: 0,
    loading: false,
    isLogin: false,
    token: '',
  });

  const getStatus = computed(() => auth.value.status);

  const getAuth = computed(() => auth.value);

  const logout = () => {
    libAuth.clearToken();

    auth.value = {
        status: 0,
        loading: false,
        isLogin: false,
        token: '',
    };
  };

  const login = async (user: any) => {
    try {
        auth.value = {
            ...auth.value,
            loading: true,
        };

        const { data } = await AuthService.login(user);
        const response = getResponse(data);
        libAuth.setToken(response.id_token);

        auth.value = {
            status: data.status,
            loading: false,
            isLogin: true,
            token: response.id_token,
        };

    } catch (error) {
        auth.value = {
            status: 0,
            loading: false,
            isLogin: false,
            token: '',
        };
    }
  };

  return {
    auth,
    getAuth,
    getStatus,
    login,
    logout,
}
});

export default useAuthStore;