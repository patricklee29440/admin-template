import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

const useUserStore = defineStore('user', () => {
  const users = ref([]);

  const getUsers = computed(() => users.value);

  const addUser = () => {

  };
  return { users, getUsers, addUser }
});

export default useUserStore;