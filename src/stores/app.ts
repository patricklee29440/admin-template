import { ref, computed } from 'vue';
import { defineStore } from 'pinia';

export interface NotificationType {
  id: string;
  type: 'error' | 'success';
  message: string;
  description: string;
  title: string;
};

export interface AppType {
  notification: Array<NotificationType>
}

const useAppStore = defineStore('app', () => {
  const app = ref<AppType>({
    notification: [],
  });

  const getNotification = computed(() => app.value.notification);

  const addNotification = (notification: NotificationType) => {
    app.value.notification.push({
        ...app.value.notification,
        ...notification,
        id: Date.now().toString(),
    });
  };

  const removeNotification = (id: string) => {
    app.value.notification = app.value.notification.filter(item => {
        return item.id !== id;
    });
  };

  return { app, getNotification, addNotification, removeNotification }
});

export default useAppStore;